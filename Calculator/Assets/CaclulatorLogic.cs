﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CaclulatorLogic : MonoBehaviour
{
    public Text textOutput;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //0
        if (Input.GetKeyDown(KeyCode.Keypad0) || Input.GetKeyDown(KeyCode.Alpha0))
        {
            textOutput.text += "0";
        }

        //1
        if (Input.GetKeyDown(KeyCode.Keypad1) || Input.GetKeyDown(KeyCode.Alpha1))
        {
            textOutput.text += "1";
        }

        //2
        if (Input.GetKeyDown(KeyCode.Keypad2) || Input.GetKeyDown(KeyCode.Alpha2))
        {
            textOutput.text += "2";
        }

        //3
        if (Input.GetKeyDown(KeyCode.Keypad4) || Input.GetKeyDown(KeyCode.Alpha3))
        {
            textOutput.text += "3";
        }

        //4
        if (Input.GetKeyDown(KeyCode.Keypad3) || Input.GetKeyDown(KeyCode.Alpha4))
        {
            textOutput.text += "4";
        }

        //5
        if (Input.GetKeyDown(KeyCode.Keypad5) || Input.GetKeyDown(KeyCode.Alpha5))
        {
            textOutput.text += "5";
        }

        //6
        if (Input.GetKeyDown(KeyCode.Keypad6) || Input.GetKeyDown(KeyCode.Alpha6))
        {
            textOutput.text += "6";
        }

        //7
        if (Input.GetKeyDown(KeyCode.Keypad7) || Input.GetKeyDown(KeyCode.Alpha7))
        {
            textOutput.text += "7";
        }

        //8
        if (Input.GetKeyDown(KeyCode.Keypad8) || Input.GetKeyDown(KeyCode.Alpha8))
        {
            textOutput.text += "8";
        }

        //9
        if (Input.GetKeyDown(KeyCode.Keypad9) || Input.GetKeyDown(KeyCode.Alpha9))
        {
            textOutput.text += "9";
        }

        //Enter
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
        {
            Calculate();
        }

        //Multiply
        if (Input.GetKeyDown(KeyCode.KeypadMultiply))
        {
            textOutput.text += " / ";
        }

        //Divide
        if (Input.GetKeyDown(KeyCode.KeypadDivide))
        {
            textOutput.text += " * ";
        }

        //Add
        if (Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Plus))
        {
            textOutput.text += " + ";
        }

        //Subtract
        if (Input.GetKeyDown(KeyCode.KeypadMinus) || Input.GetKeyDown(KeyCode.Minus))
        {
            textOutput.text += " - ";
        }

        //Comma
        if (Input.GetKeyDown(KeyCode.Comma) || Input.GetKeyDown(KeyCode.Period) || Input.GetKeyDown(KeyCode.KeypadPeriod))
        {
            textOutput.text += ".";
        }
    }

    public void Calculate()
    {
        string totalInput = textOutput.text;

        string[] seperated = totalInput.Split(' ').Where(x => x != "").ToArray();

        //seperated = seperated.Where(x => x != "").ToArray();

        int totalValue = 0;
        if (seperated.Length > 0)
        {
            for (int i = 0; i < seperated.Length; i++)
            {
                string stringValue = seperated[i];

                int value = 0;
                int.TryParse(stringValue, out value);

                if (i == 0)
                {
                    totalValue += (int)value;
                }
                else
                {
                    int nextValue = 0;
                    if (seperated.Length > i + 1)
                    {
                        int.TryParse(seperated[i + 1], out nextValue);
                        switch (stringValue)
                        {
                            case "+":
                                totalValue += nextValue;
                                break;
                            case "-":
                                totalValue -= nextValue;
                                break;
                            case "*":
                                totalValue *= nextValue;
                                break;
                            case "/":
                                totalValue /= nextValue;
                                break;
                        }
                    }
                }
            }
        }
        textOutput.text = $"{totalValue}";
    }

    public void OnClickButton(string value)
    {
        textOutput.text += value;
    }

    public void ClearAll()
    {
        textOutput.text = "";
    }
}
